# @kucrut/svelte-stuff

## 0.1.2

### Patch Changes

- 4d54105: Update dependencies

## 0.1.1

### Patch Changes

- 766dd3d: Svelte 5, ESLint 9

## 0.1.0

### Minor Changes

- f1b9c5b: Improve actions

### Patch Changes

- 0698881: SvelteKit 2
- 244697f: Fix CI

## 0.0.5

### Patch Changes

- 9767ffc: Update dependencies

## 0.0.4

### Patch Changes

- c8ef360: Fix packaging

## 0.0.3

### Patch Changes

- 6741295: Svelte v4

## 0.0.2

### Patch Changes

- d98e068: Fix action exports aggregation

## 0.0.1

### Patch Changes

- 9bee6ca: Zero Zero Three
- aaca4a1: Zero Zero Four
- 8aca651: Zero Zero Five
- 8d385fa: Zero Zero Two
- d89a0fb: Zero Zero One
