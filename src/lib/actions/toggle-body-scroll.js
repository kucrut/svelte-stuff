/**
 * @typedef {{
 *   class_name?: string;
 *   css_prop_scroll?: string;
 *   css_prop_body_width?: string;
 *   should_disable: boolean;
 * }} Params
 * @typedef {import('svelte/action').ActionReturn<Params>} ActionReturn
 */

/**
 * Manipulate scrollbar
 *
 * When the sidebar is open, scrollbar should be disabled so that
 * scrolling on the sidebar doesn't affect the rest of the page.
 *
 * @param {HTMLBodyElement} body   Body element.
 * @param {Params}          params Initial action parameters.
 * @return {ActionReturn} Object containing update and destroy callbacks.
 */
export function toggle_body_scroll( body, params ) {
	/** @type {Required<Params>} */
	const defaults = {
		class_name: 'no-scroll',
		css_prop_scroll: '--scroll-y',
		css_prop_body_width: '--body-width',
		should_disable: false,
	};

	/** @type {Required<Params>} */
	let current_params = { ...defaults, ...params };

	function disable() {
		body.parentElement?.style.setProperty( current_params.css_prop_scroll, window.scrollY.toString() );
		body.parentElement?.style.setProperty( current_params.css_prop_body_width, body.clientWidth + 'px' );
		body.classList.add( current_params.class_name );
	}

	function enable() {
		const scrollY = body.parentElement?.style.getPropertyValue( current_params.css_prop_scroll );

		body.classList.remove( current_params.class_name );
		window.scrollTo( 0, Number( scrollY ) );
	}

	/**
	 * Toggle scrollbar
	 *
	 * @param {Params} next_params Action parameters.
	 */
	function toggle( next_params ) {
		current_params = { ...defaults, ...next_params };

		if ( current_params.should_disable ) {
			disable();
		} else {
			enable();
		}
	}

	toggle( params );

	return {
		update: toggle,
		destroy: () => toggle( { should_disable: false } ),
	};
}
