/** @typedef {{ active: boolean; callback: () => void; }} Params */
/** @typedef {import('svelte/action').ActionReturn<Params>} ActionReturn */

/**
 * Handle click event outside of an element
 *
 * @param {HTMLElement} node   Element.
 * @param {Params}      params Action parameters.
 * @return {ActionReturn} Action object.
 */
export function click_outside( node, params ) {
	/** @type {Params[ 'callback' ]} */
	let current_callback;

	/**
	 * Handle click event
	 *
	 * @param {MouseEvent} event Click event.
	 */
	const handle_click = event => {
		if ( event.target instanceof HTMLElement && ! node.contains( event.target ) ) {
			current_callback();
		}
	};

	/**
	 * Toggle event listener
	 *
	 * @param {Params} current_params Current action parameters.
	 */
	const toggle = current_params => {
		const { active, callback } = current_params;

		if ( active ) {
			current_callback = callback;
			document.documentElement.addEventListener( 'click', handle_click, true );
		} else {
			document.documentElement.removeEventListener( 'click', handle_click, true );
		}
	};

	toggle( params );

	return {
		/**
		 * Update
		 *
		 * @param {Params} next_params Next action parameters.
		 */
		update( next_params ) {
			toggle( next_params );
		},

		destroy() {
			toggle( { active: false, callback: current_callback } );
		},
	};
}
