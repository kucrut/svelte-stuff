/** @type {{active?: boolean}} */
const defaults = { active: true };

/**
 * Trap focus
 *
 * @param {HTMLElement}      node   Element.
 * @param {typeof defaults=} params Action parameters.
 *
 * @return {import('svelte/action').ActionReturn<typeof defaults>} Action object.
 */
export function trap_focus( node, params = defaults ) {
	/** @type {Element|null} */
	let focus_el;
	let is_active = false;

	/**
	 * Handle keydown event
	 *
	 * @param {KeyboardEvent} event Event.
	 */
	function handle_keydown( event ) {
		if ( event.code !== 'Tab' ) {
			return;
		}

		event.preventDefault();

		const tabbables = Array.from( node.querySelectorAll( '*' ) ).filter( el => {
			return (
				el instanceof HTMLElement &&
				'tabIndex' in el &&
				el.tabIndex >= 0 &&
				! el.hasAttribute( 'disabled' ) &&
				! el.hasAttribute( 'hidden' ) &&
				! el.getAttribute( 'aria-hidden' )
			);
		} );

		if ( ! tabbables.length ) {
			return;
		}

		// Index of element that's currently in focus.
		let index = node.ownerDocument.activeElement ? tabbables.indexOf( node.ownerDocument.activeElement ) : -1;

		if ( event.shiftKey ) {
			index = index > 0 ? index - 1 : tabbables.length - 1;
		} else {
			index = index + 1 < tabbables.length ? index + 1 : 0;
		}

		const focused_el = tabbables[ index ];

		if ( focused_el instanceof HTMLElement ) {
			focused_el.focus();
		}
	}

	/**
	 * Toggle keydown event listener
	 *
	 * @param {boolean} should_listen State.
	 */
	function toggle_listener( should_listen ) {
		if ( should_listen ) {
			window.addEventListener( 'keydown', handle_keydown );
		} else {
			window.removeEventListener( 'keydown', handle_keydown );
		}

		is_active = should_listen;
	}

	function activate() {
		toggle_listener( true );
		focus_el = node.ownerDocument.activeElement;
		node.focus();
	}

	function deactivate() {
		toggle_listener( false );

		if ( focus_el && focus_el instanceof HTMLElement ) {
			focus_el.focus();
		}

		focus_el = null;
	}

	if ( params.active ) {
		activate();
	}

	return {
		destroy() {
			deactivate();
		},

		update( update_params ) {
			if ( ! is_active && update_params.active ) {
				activate();
			} else if ( is_active && ! update_params.active ) {
				deactivate();
			}
		},
	};
}
