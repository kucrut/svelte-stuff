/** @typedef {{ active: boolean; callback?: () => void }} Params */

/**
 * Handle escape keydown event
 *
 * @param {Element} _      Element (unused).
 * @param {Params}  params Action parameters.
 * @return {import('svelte/action').ActionReturn<Params>} Action object
 */
export function handle_escape( _, params ) {
	/** @type {Params|undefined} */
	let state;

	function deactivate() {
		if ( state?.active ) {
			window.removeEventListener( 'keydown', handle_keydown, true );
		}
	}

	/**
	 * Handle keydown event
	 *
	 * @param {KeyboardEvent} event Keyboard event.
	 */
	function handle_keydown( event ) {
		if ( typeof state?.callback === 'function' && event.code === 'Escape' ) {
			state.callback();
		}
	}

	/**
	 * Toggle event listener
	 *
	 * @param {Params} current_params Current action parameters.
	 */
	function toggle( current_params ) {
		deactivate();

		if ( current_params.active && typeof current_params.callback === 'function' ) {
			window.addEventListener( 'keydown', handle_keydown, true );
		}

		state = { ...current_params };
	}

	toggle( params );

	return {
		destroy() {
			deactivate();
		},

		update( next_params ) {
			toggle( next_params );
		},
	};
}
