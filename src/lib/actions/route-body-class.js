/**
 * @typedef {{
 *   class_name_prefix?: string;
 *   home_class_name?: string;
 *   path: string;
 * }} Params
 * @typedef {import('svelte/action').ActionReturn<Params>} ActionReturn
 */

/**
 * Set body element class based on current route
 *
 * @param {HTMLElement} node   Body element.
 * @param {Params}      params Initial action parameters.
 * @return {ActionReturn} Object containing update and destroy callbacks.
 */
export function route_body_class( node, params ) {
	/** @type {string[]} */
	let current_classes = [];

	function destroy() {
		if ( current_classes.length > 0 ) {
			current_classes.forEach( cls => node.classList.remove( cls ) );
		}
	}

	/**
	 * Set class
	 *
	 * @param {Params} current_params Action parameters.
	 */
	function set_class( { class_name_prefix, home_class_name, path } ) {
		destroy();

		const home_cls = typeof home_class_name === 'string' && home_class_name ? home_class_name : 'home';
		const prefix = typeof class_name_prefix === 'string' && class_name_prefix ? class_name_prefix : '';
		const parts = path
			.trim()
			.split( '/' )
			.filter( i => i !== '' );

		if ( parts.length > 0 ) {
			current_classes = parts.map( ( _, i, arr ) => prefix + arr.slice( 0, i + 1 ).join( '-' ) );
		} else {
			current_classes = [ `${ prefix }${ home_cls }` ];
		}

		current_classes.forEach( cls => node.classList.add( cls ) );
	}

	set_class( params );

	return {
		destroy,
		update: set_class,
	};
}
